#r @"LambdaWorks.Parallel.dll"

open LambdaWorks.Parallel

let time f =
   let start = System.DateTime.Now.Ticks
   let v     = f ()
   (System.DateTime.Now.Ticks - start, v)

let hogCpuFunN N x my =
  let rec aux n acc = 
    if n = N then acc
    else aux (n + 1) (acc + 1.0 / (float N) * ((x - my)**2.0) - acc)
  aux 1 0.0

let dev (hog : double -> double -> double) a =
  let my = Array.average a
  a |> Array.fold (fun a x -> a + (hog x my)) 0.0

let parallelDev1 hog a =
  let length    = Array.length a
  let chunkSize = length / System.Environment.ProcessorCount
  let my        = 
    (length, chunkSize) ||> Array.chunkIndices 
                         |> Array.map (fun (start, stop) -> async {return (Array.Parallel.foldSub start stop (fun a x -> a + x) 0.0 a)})
                         |> Async.Parallel
                         |> Async.RunSynchronously
                         |> Array.sum
                         |> (fun x -> x / (double length))
  (length, chunkSize) ||> Array.chunkIndices
                       |> Array.map (fun (start, stop) -> async {return (Array.Parallel.foldSub start stop (fun a x -> a + (hog x my)) 0.0 a)})
                       |> Async.Parallel
                       |> Async.RunSynchronously
                       |> Array.sum


let parallelDev2 hog a = 
  let my = a |> Array.Parallel.Interlocked.fold (fun a x -> a + x) 0.0 Array.sum |> (fun x -> x / (double (Array.length a)))
  Array.Parallel.Interlocked.fold (fun a x -> a + hog x my) 0.0 Array.sum a

let parallelDev3 hog a =
  let my = a |> Array.Parallel.fold (fun a x -> a + x) 0.0 Array.sum |> (fun x -> x / (double (Array.length a)))
  Array.Parallel.fold (fun a x -> a + hog x my) 0.0 Array.sum a


let dataSerial size =
  let rng = new System.Random()
  Array.init size (fun _ -> rng.NextDouble())

let dataParallel size = 
  let chunkSize            = size / System.Environment.ProcessorCount
  let data                 = (Array.zeroCreate size : double array)
  let sample (start, stop) = async {
    let rng = new System.Random(); 
    return (Array.Parallel.iterSub start stop (fun _ i -> data.[i] <- rng.NextDouble()) data)
  }
  let () = (size, chunkSize) ||> Array.chunkIndices
                                |> Array.map sample
                                |> Async.Parallel
                                |> Async.RunSynchronously
                                |> ignore
  data


let sample = dataParallel 100000000

(*
let (ticks, value) = time (fun () -> dev sample) in
  Printf.printf "ticks = %u, value = %f\n" ticks value
*)

[for i in 1..10 -> i] |> Seq.iter (fun N ->
  let hog = hogCpuFunN N in
  let (ticks1, value) = time (fun () -> parallelDev1 hog sample) in
  let () = Printf.printf "Subarray:     ticks = %u, value = %f\n" ticks1 value in
  let (ticks2, value) = time (fun () -> parallelDev2 hog sample) in
  let () = Printf.printf "Interlocked:  ticks = %u, value = %f\n" ticks2 value in
  let (ticks3, value) = time (fun () -> parallelDev3 hog sample) in
  let () = Printf.printf "Subarray new: ticks = %u, value = %f\n" ticks3 value in
  Printf.printf "-----------------------------------------------\n"
)
